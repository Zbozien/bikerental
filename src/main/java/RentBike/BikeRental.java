package RentBike;

import java.util.*;

public class BikeRental {

    private Scanner scanner = new Scanner(System.in);
    private List<Bike> bikes;
    private List<Bike> myBikes = new ArrayList();
    private double money;

    void startRental() {
        System.out.println("Witam w wypożyczalni rowerów");
        System.out.println("Ile pieniędzy chcesz przeznaczyć na wypożuczenie roweru?");
        money = Integer.parseInt(scanner.nextLine());
        BikeData bikeData = new BikeData();
        bikes = bikeData.showBikes();
        showAllBikes(bikes);
        System.out.println();
        instructions();
        choices();
    }

    private void instructions() {
        System.out.println("Aby wykonac żądaną operacje wpisz na ekranie: \n - koniec - konczy działanie programu i wyświetla podsumowanie" +
                "\n - zarezerwuj oraz wpisz numer roweru - rezerwuje wybrany rower \n - pokaz rowery - pokazuje wszystkie rowery" +
                "\n - moje rowery - pokazuje zarezerwowane rowery \n - filtruj - nastepnie wpisz kolor, marka, cena lub status" +
                "\n - doladuj - zasilenie konta pieniedzmi");

    }

    private void choices() {

        String choice = scanner.nextLine();
        String[] choiceArray = choice.split(" ");

        if (choice.equalsIgnoreCase("koniec")) {
            System.out.println("Podsumowanie: ");
            Date actualDate = new Date();
            for (Bike bike : myBikes) {
                double seconds = differenceInSeconds(bike.getRentdate(), actualDate);
                double price = seconds / 10 * bike.getPrice();
                System.out.println(bike);
                System.out.println("Rower miałeś wynajęty przez " + seconds / 10 + " godzin i kosztował: " + price + " złotych");
                money -= price;
                bike.setAvailable(true);

            }

            myBikes = new ArrayList();
            System.out.println("Reszta pieniędzy: " + money + " złotych");
            if (money >= 0) {
                return;
            } else {
                System.out.println("Nie masz pieniędzy, doładuj konto");
            }
        }

        if (choiceArray[0].equalsIgnoreCase("zarezerwuj")) {
            int i = Integer.parseInt(choiceArray[1]);
            Bike bike = bikes.get(i - 1);
            if (!bike.getAvailable()) {
                System.out.println("Wybrany rower jest już zajęty");
            } else {
                bike.rent();
                myBikes.add(bike);
                System.out.println("Rower został zarezerwowany");
            }
        }

        if (choice.equalsIgnoreCase("pokaz rowery")) {
            showAllBikes(bikes);
        }

        if (choice.equalsIgnoreCase("moje rowery")) {
            showAllBikes(myBikes);
        }

        if (choiceArray[0].equalsIgnoreCase("filtruj")) {
            if (choiceArray[1].equalsIgnoreCase("kolor")) {
                filterBikeByColor(bikes, choiceArray[2]);
            }
            if (choiceArray[1].equalsIgnoreCase("marka")) {
                filterBikeByBrand(bikes, choiceArray[2]);
            }

            if (choiceArray[1].equalsIgnoreCase("cena")) {
                filterBikeByPrice(bikes);
            }

            if (choiceArray[1].equalsIgnoreCase("status")) {
                filterBikeByStatus(bikes, choiceArray[2]);
            }
        }

        if (choiceArray[0].equalsIgnoreCase("doladuj")) {
            money = money + Integer.parseInt(choiceArray[1]);
            System.out.println("Twoje konto zostało zasilone, Twój obecny stan konta wynosi:" + money + " złotych");
        }

        choices();
    }

    private void showAllBikes(List<Bike> bikes) {
        for (int i = 0; i < bikes.size(); i++) {
            System.out.println((i + 1) + ": " + bikes.get(i).toString());
        }
    }

    private void filterBikeByColor(List<Bike> bikes, String color) {
        for (int i = 0; i < bikes.size(); i++) {
            if (bikes.get(i).getColor().equals(color)) {
                System.out.println((i + 1) + ": " + bikes.get(i).toString());
            }
        }
    }

    private void filterBikeByBrand(List<Bike> bikes, String brand) {
        for (int i = 0; i < bikes.size(); i++) {
            if (bikes.get(i).getBrand().equals(brand)) {
                System.out.println((i + 1) + ": " + bikes.get(i).toString());
            }
        }
    }

    private void filterBikeByPrice(List<Bike> bikes) {
        bikes.sort(Comparator.comparing(Bike::getPrice));
        for (int i = 0; i < bikes.size(); i++) {
            System.out.println((i + 1) + ": " + bikes.get(i).toString());
        }
    }

    private void filterBikeByStatus(List<Bike> bikes, String status) {
        for (int i = 0; i < bikes.size(); i++) {
            if (bikes.get(i).getAvailable().toString().equals(status)) {
                System.out.println((i + 1) + ": " + bikes.get(i).toString());
            }
        }
    }

    private double differenceInSeconds(Date date1, Date date2) {
        return (date2.getTime() - date1.getTime()) / 1000;
    }

}
