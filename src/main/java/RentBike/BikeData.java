package RentBike;

import java.util.List;

import static java.util.Arrays.asList;

public class BikeData {

    public List<Bike> showBikes() {

        List<Bike> bikes = asList(
                new Bike("Rower1", "KROSS", "czerwony", 4, false),
                new Bike("Rower2", "KROSS", "czerwony", 4, true),
                new Bike("Rower3", "Merida", "czarny", 3, false),
                new Bike("Rower4", "Merida", "czarny", 5, true),
                new Bike("Rower5", "Kands", "niebieski", 10, false),
                new Bike("Rower6", "Kands", "niebieski", 8, true),
                new Bike("Rower7", "Giant", "czarny", 6, false),
                new Bike("Rower8", "Giant", "zielony", 5, true),
                new Bike("Rower9", "Romet", "zielony", 2, false),
                new Bike("Rower10", "Btwin", "niebieski", 5, true)
        );
        return bikes;
    }
}
