package RentBike;

import java.util.Date;

public class Bike {
    private String name;
    private String brand;
    private String color;
    private Integer price;
    private Boolean available;
    private Date rentdate;

    public Bike(String name, String brand, String color, Integer price, Boolean available) {
        this.name = name;
        this.brand = brand;
        this.color = color;
        this.price = price;
        this.available = available;
    }

    public void rent() {
        available = false;
        rentdate = new Date();
    }

    public Date getRentdate() {
        return rentdate;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public Integer getPrice() {
        return price;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", available=" + available +
                ", rentdate=" + rentdate +
                '}';
    }
}
